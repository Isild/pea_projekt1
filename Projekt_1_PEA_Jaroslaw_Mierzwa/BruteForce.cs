﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//using System.Diagnostics;

/// <summary>
/// Projektowanie efektywnych algorytmów : projekt 1
/// Autor: Przemysław Wujek, 234983
/// Prowadzący: Dr inż. Jarosław Mierzwa
/// Termin: Czwartek 7:30
/// 
/// Przegląd zupełny.
/// </summary>

namespace Projekt_1_PEA_Jaroslaw_Mierzwa
{
    class BruteForce
    {
        public Graph g;
        protected int tmpCost; //koszt obecnie wyznaczanej ścieżki
        protected int[] tmpRoute; //obecna ścieżka
        //-------------------------------------------------------------------------------------
        protected bool[] visitedCities; //odwiedzone miasta po drodze
        protected int startTop; //wierzchołek początkowy
        //-------------------------------------------------------------------------------------
        protected int bestCost; //koszt najlepszej wyznaczonej ścieżki
        protected int[] bestRoute; //najlepsza ścieżka(o najmniejszym koszcie
        //-------------------------------------------------------------------------------------
        private int whichCity;//które miasto z kolei odwiedzone, zmienna pomocnicza
        public int iteration;
        //-------------------------------------------------------------------------------------

        /// <summary>
        /// Konstruktor, przypisuje graf.
        /// Przygotowuje tablice do algorytmu.
        /// </summary>
        /// <param name="g"> - graf na którym jest przeprowadzany algorytm.</param>
        public BruteForce(Graph gr)
        {
            g = gr;
            //this.cityMatrix = g.cityMatrix;
            //this.neighborhoodMatrix = g.neighborhoodMatrix;
            //this.size = g.size;
            //stworzenie nowych tablic potrzebnych do algorytmu
            tmpRoute = new int[g.size];
            visitedCities = new bool[g.size];
            bestRoute = new int[g.size];

            bestCost = int.MaxValue;
            startTop = 0;
            tmpCost = 0;
            whichCity = 1;
            iteration = 0;
            for (int i = 0; i < g.size; i++)
            {
                tmpRoute[i] = -1;
                visitedCities[i] = false;
                bestRoute[i] = -1;
            }

            //zakładam że zaczynam w wierzchołku nr 0 
            //visitedCities[v] = true;
            //tmpRoute[v] = 0;
            //whichCity++;
        }

        /// <summary>
        /// Funkcja szukająca cyklu Hamiltona o najmniejszym koszcie. TSP
        /// </summary>
        /// <param name="v"> - wierzchołek z którego jest dokonywany ruch.</param>
        public void FindRoute(int v)
        {
            if(iteration == 0)
            {
                visitedCities[v] = true;
                tmpRoute[v] = 0;
            }
            iteration++;
            if(!isAllCitiesVisited())
            {
                visitedCities[v] = true;//można to umieścić w tworzeniu funkcji, chociaż wierzchołek początkowy można wybrać losowo

                for (int c = 0; c < g.size; c++)//sprawdzam czy dane miasto nie było już odwiedzone
                {
                    if (!visitedCities[c] && g.cityMatrix[v, c] > 0 && c != v)
                    {
                        tmpCost += g.cityMatrix[v, c];
                        visitedCities[c] = true;
                        tmpRoute[whichCity++] = c;
                        //Console.WriteLine("v: " + v + ", c: " + c + ", koszt: " + cityMatrix[v, c] + ", aktualny koszt: " + tmpCost);
                        FindRoute(c);

                        tmpCost -= g.cityMatrix[v, c];//po odwiedzeniu miasta
                        visitedCities[c] = false;
                        whichCity--;
                    }
                }
            }
            else
            {
                tmpCost += g.cityMatrix[tmpRoute[g.size - 1], tmpRoute[0]];
                /*Console.Write("Znaleziony cykl Hamiltona: ");
                for (int i = 0; i < size; i++)
                {
                    if (i < size - 1)
                        Console.Write(tmpRoute[i] + " -> ");
                    else
                        Console.Write(tmpRoute[i] + " -> " + tmpRoute[0] + "\n");
                }
                Console.WriteLine("Koszt: " + tmpCost);//*/


                if (tmpCost < bestCost) //zapisanie najlepszej drogi i jej wartości
                {
                    bestCost = tmpCost;

                    for (int i = 0; i < g.size; i++) 
                        bestRoute[i] = tmpRoute[i];
                }

                tmpCost -= g.cityMatrix[tmpRoute[g.size - 1], tmpRoute[0]];
            }
        }
        /// <summary>
        /// Funkcja znalezienia drogi, ulepszona o porównanie kosztu aktualnej drogi do najlepszej drogi.
        /// </summary>
        /// <param name="v"> - wierzchołek początkowy</param>
        public void FindRouteBetter(int v)
        {
            if (iteration == 0)
            {
                visitedCities[v] = true;
                tmpRoute[v] = 0;
            }
            iteration++;
            if (!isAllCitiesVisited())
            {
                visitedCities[v] = true;//można to umieścić w tworzeniu funkcji, chociaż wierzchołek początkowy można wybrać losowo

                for (int c = 0; c < g.size; c++)//sprawdzam czy dane miasto nie było już odwiedzone
                {
                    if (!visitedCities[c] && g.cityMatrix[v, c] > 0 && c != v && tmpCost < bestCost)
                    {
                        tmpCost += g.cityMatrix[v, c];
                        visitedCities[c] = true;
                        tmpRoute[whichCity++] = c;
                        //Console.WriteLine("v: " + v + ", c: " + c + ", koszt: " + cityMatrix[v, c] + ", aktualny koszt: " + tmpCost);
                        FindRoute(c);

                        tmpCost -= g.cityMatrix[v, c];//po odwiedzeniu miasta
                        visitedCities[c] = false;
                        whichCity--;
                    }
                }
            }
            else
            {
                tmpCost += g.cityMatrix[tmpRoute[g.size - 1], tmpRoute[0]];

                if (tmpCost < bestCost) //zapisanie najlepszej drogi i jej wartości
                {
                    bestCost = tmpCost;

                    for (int i = 0; i < g.size; i++)
                        bestRoute[i] = tmpRoute[i];
                }

                tmpCost -= g.cityMatrix[tmpRoute[g.size - 1], tmpRoute[0]];
            }
        }

        /// <summary>
        /// Funkcja sprawdzająca czy wszystkie miasta zostały odwiedzone.
        /// </summary>
        /// <returns>True - gdy wszystko zostało odwiedzone, False - gdy jakieś miasto zostało nieodwiedzone.</returns>
        protected bool isAllCitiesVisited()
        {
            for (int i = 0; i < g.size; i++)
            {
                if (!visitedCities[i])
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Funkcja wyświetlająca całą drogę najkrótszego cyklu Hamiltonowskiego, kończy się na punkcie początkowym. 
        /// </summary>
        public void showBestWay()
        {
            Console.WriteLine("Najkrótszy cykl Hamiltonowski o koszcie: " + bestCost);
            for (int i = 0; i < g.size; i++) 
            {
                if (i < g.size - 1) 
                    Console.Write(bestRoute[i] + " -> ");
                else
                    Console.Write(bestRoute[i] + " -> " + bestRoute[0] + "\n");
            }
        }
    }
}
