﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Projektowanie efektywnych algorytmów : projekt 1
/// Autor: Przemysław Wujek, 234983
/// Prowadzący: Dr inż. Jarosław Mierzwa
/// Termin: Czwartek 7:30
/// 
/// Programowanie dynamiczne.
/// Polega na podzieleniu dużego problemu na podproblemy.
/// https://www.youtube.com/watch?v=JE0JE8ce1V0&app=desktop
/// </summary>

namespace Projekt_1_PEA_Jaroslaw_Mierzwa
{
    class DynamicProgramming
    {
        public Graph g;
        protected int tmpCost; //koszt obecnie wyznaczanej ścieżki
        protected int[] tmpRoute; //obecna ścieżka
        //-------------------------------------------------------------------------------------
        protected bool[] visitedCities; //odwiedzone miasta po drodze
        protected int startTop; //wierzchołek początkowy
        //-------------------------------------------------------------------------------------
        protected int bestCost; //koszt najlepszej wyznaczonej ścieżki
        protected int[] bestRoute; //najlepsza ścieżka(o najmniejszym koszcie
        //-------------------------------------------------------------------------------------
        private int whichCity;//które miasto z kolei odwiedzone, zmienna pomocnicza
        public int iteration;
        //-------------------------------------------------------------------------------------

        int maxMask;//maksymalna maska odwiedzeń wierzchołków

        /// <summary>
        /// Konstruktor, przypisuje graf.
        /// Przygotowuje tablice do algorytmu.
        /// </summary>
        /// <param name="g"> - graf na którym jest przeprowadzany algorytm.</param>
        public DynamicProgramming(Graph gr)
        {
            g = gr;
            //this.cityMatrix = g.cityMatrix;
            //this.neighborhoodMatrix = g.neighborhoodMatrix;
            //this.size = g.size;
            //stworzenie nowych tablic potrzebnych do algorytmu
            tmpRoute = new int[g.size];
            visitedCities = new bool[g.size];
            bestRoute = new int[g.size];

            bestCost = int.MaxValue;
            startTop = 0;
            tmpCost = 0;
            whichCity = 1;
            iteration = 0;
            for (int i = 0; i < g.size; i++)
            {
                tmpRoute[i] = -1;
                visitedCities[i] = false;
                bestRoute[i] = -1;
            }

            //zakładam że zaczynam w wierzchołku nr 0 
            //visitedCities[v] = true;
            //tmpRoute[v] = 0;
            //whichCity++;
            visitedCities[0] = true;
            maxMask = (int)Math.Pow(2, g.size) - 1;//maksymalna maska odwiedzeń wierzchołków
        }

        public int DynamicFunction(int v)
        {
            int c = v;
            if(v<maxMask)
            {
                int dv = 0;
                int spr = 1;
                for (int i = 0; i < g.size; i++)
                {
                    dv = 0;
                    spr = 1;//wartość pomocnicza sprawdzająca co będzie na bitach
                    for (int j = 0; j < i; j++)
                    {
                        spr = spr << 0b1;
                    }
                    dv |= (v & spr);//pobieram konkretne wartości z bitów

                    if (dv == 0b0)
                    {
                        DynamicFunction(v | spr);
                    }
                }

                int b = 1;
                for (int i = 0; i < g.size; i++)
                {
                    b = b << 0b1; //przesówam jeden bit tyle razy w lewo aby był on nr wierzchołka
                }

                c = c & b; //dokładam do zbioru odwiedzony wierzchołek

                return g.cityMatrix[v, c] += DynamicFunction(c);
            }
            else
            {
                return g.cityMatrix[v, c];
            }
            

            
            
            return 1;
        }
    }
}
