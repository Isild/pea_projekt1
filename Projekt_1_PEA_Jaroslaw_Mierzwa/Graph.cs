﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Text.RegularExpressions;


/// <summary>
/// Projektowanie efektywnych algorytmów : projekt 1
/// Autor: Przemysław Wujek, 234983
/// Prowadzący: Dr inż. Jarosław Mierzwa
/// Termin: Czwartek 7:30
/// </summary>

namespace Projekt_1_PEA_Jaroslaw_Mierzwa
{
    public class Graph
    {
        public int size;//ilość miast
        public int[,] cityMatrix;//droga do każdego miasta
        public bool[,] neighborhoodMatrix;//sąsiedztwo miast
        

        /// <summary>
        /// Konstruktor grafu wczytujący dane z pliku w formacie txt.
        /// </summary>
        /// <param name="fileName"> - nazwa pliku, z którego wczytujemy dane.</param>
        /// <param name="g"> - obiekt grafu.</param>
        public Graph(string fileName, Graph g)
        {
            readFromFile(fileName);
            CreateMap();
            //drawMatrix();

            for (int i = 0; i < size; i++) //aby tablica miała na przekątnej -1
                for (int j = 0; j < size; j++)
                    if (i == j)
                        cityMatrix[i, j] = -1;
        }

        public Graph(Graph g)
        {
            size = g.size;
            cityMatrix = new int[size, size];
            CreateMap();

            //Console.WriteLine("graf");
            //g.drawMatrix();

            for (int i = 0; i < size; i++)
                for (int j = 0; j < size; j++)
                    if (i == j)
                        cityMatrix[i, j] = -1;
            //drawMatrix();
        }

        /// <summary>
        /// Konstruktor grafu generujący graf, gdy mamy podaną ilość miast oraz zakres wag na krawędziach.
        /// </summary>
        /// <param name="s"> - ilość miast w grafie.</param>
        /// <param name="max"> - maksymalna wartość wagi krawędzi.</param>
        /// <param name="min"> - minimalna wartość wagi krawędzi.</param>
        /// <param name="symetric"> - wartość boolowska, true jeśli graf ma być symetryczny, false jeśli ma być niesymetryczny.</param>
        public Graph(int s, int maxW, int minW, bool symetric)
        {
            Random rnd = new Random();
            size = s;

            cityMatrix = new int[size, size];
            
            if (!symetric)
            {
                //graf asymetryczny
                for (int i = 0; i < size; i++)
                {
                    for (int j = 0; j < size; j++)
                    {
                        if (i == j)
                        {
                            cityMatrix[i, j] = -1;//wartość na przekątnej, połączenie z samym sobą
                        }
                        else
                        {
                            int r1 = rnd.Next(minW, maxW);
                            int r2 = rnd.Next(minW, maxW);
                            cityMatrix[i, j] = r1;
                            cityMatrix[j, i] = r2;
                        }
                    }
                }
            }
            else
            {
                //graf symetryczny
                for (int i = 0; i < size; i++)
                {
                    for (int j = 0; j < i; j++)//poruszanie się pod przekątną, wyżej nie potrzeba
                    {
                        if (i == j)
                        {
                            cityMatrix[i, j] = -1;//wartość na przekątnej, połączenie z samym sobą
                        }
                        else
                        {
                            int r = rnd.Next(minW, maxW);
                            cityMatrix[i, j] = r;
                            cityMatrix[j, i] = r;
                        }
                    }
                }
            }

            CreateMap();
        }

        public int getSize()
        {
            return this.size;
        }

        private void CreateMap()
        {
            neighborhoodMatrix = new bool[size, size];

            for (int j = 0; j < size; j++)
            {
                for (int i = 0; i < size; i++)
                {
                    if (cityMatrix[i, j] > 0)
                        neighborhoodMatrix[i, j] = true;
                    else
                        neighborhoodMatrix[i, j] = false;
                }
            }
        }

        /// <summary>
        /// Funkcja wczytująca dane z pliku o rozszerzeniu txt.
        /// </summary>
        /// <param name="nazwa">-nazwa pliku, z którego chcemy odczytać dane.</param>
        public void readFromFile(string nazwa)
        {
            string line;
            //string nazwa = "text123";
            int counter = 0;

            if (!nazwa.Contains(".txt"))
            {
                int s = nazwa.Length;
                nazwa = nazwa.Insert(s, ".txt");
            }

            if (File.Exists(nazwa))
            {
                System.IO.StreamReader file = new System.IO.StreamReader(@"" + nazwa);
                int a = 0, b = 0;//pozycje wprowadzanych miast w tablicy[a,b]

                while ((line = file.ReadLine()) != null)
                {
                    if (counter == 0) //pierwsze wczytanie, rozmiar
                    {
                        counter++;
                        if (!Int32.TryParse(line, out this.size))
                        {
                            Console.WriteLine("Nie udało się odczytać rozmiaru.");
                            break;
                        }
                        else
                        {
                            //Console.WriteLine("Rozmiar: " + this.size);
                            cityMatrix = new int[size, size];
                        }
                    }
                    else //wczytanie punktów do dynamicznej tablicy
                    {
                        //System.Console.WriteLine(line);//wczytanie jednej lini w formie stringa
                        string[] listOfNumbers = line.Split(' ');
                        int x;

                        foreach (string i in listOfNumbers)
                        {
                            if (Int32.TryParse(i, out x))
                            {
                                if (a == size)
                                {
                                    a = 0;
                                    b++;
                                }

                                //System.Console.WriteLine("wczytane elemeny: " + i + ", " + x + ", dodany w: " + a + " " + b);
                                cityMatrix[b, a] = x;
                                a++;
                            }
                        }
                        counter++;
                    }
                }
                file.Close();
                //System.Console.WriteLine("There were {0} lines.", counter);
            }
            else
                Console.WriteLine("Podany plik nie istnieje, spróbuj ponownie.");
        }


        /// <summary>
        /// Funkcja wyświetlająca wczytaną macierz
        /// </summary>
        /// <param name="size">-rozmiar wielkośći tablicy</param>
        public void drawMatrix()
        {
            Console.WriteLine("Tablica miast: ");
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    Console.Write(this.cityMatrix[i, j].ToString() + " ");
                }
                Console.WriteLine();
            }
            //Console.ReadLine();
        }

        /// <summary>
        /// Funkcja rysująca tablicę sąsiedztwa, 1-sąsiedztwo, 0-brak połączenia między miastami
        /// </summary>
        public void drawNeighborhoodMatrix()
        {
            Console.WriteLine("\n\nTablica sąsiedztwa: ");
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    if (neighborhoodMatrix[j, i])
                        Console.Write(1 + " ");
                    else
                        Console.Write(0 + " ");
                }
                Console.WriteLine();
            }
            Console.ReadLine();
        }


        public void TSP_function(int n, int v, int v0, int d, int dH, int s )
        {

        }
    }
}