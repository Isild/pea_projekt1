﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Projektowanie efektywnych algorytmów : projekt 1
/// Autor: Przemysław Wujek, 234983
/// Prowadzący: Dr inż. Jarosław Mierzwa
/// Termin: Czwartek 7:30
/// https://www.youtube.com/watch?v=1FEP_sNb62k&fbclid=IwAR3T5G1RvARaTOZXqFVfeJuIcWyxJNBKm65JzUKwsVUSGwjiSYICNRY-UNM
/// 
/// Algorytm branch and bound.
/// </summary>

namespace Projekt_1_PEA_Jaroslaw_Mierzwa
{
    class BranchAndBound
    {
        public Graph g;
        protected int tmpCost; //koszt obecnie wyznaczanej ścieżki
        //protected int[] tmpRoute; //obecna ścieżka
        //-------------------------------------------------------------------------------------
        protected bool[] visitedCities; //odwiedzone miasta po drodze
        protected int startTop; //wierzchołek początkowy
        //-------------------------------------------------------------------------------------
        protected int bestCost; //koszt najlepszej wyznaczonej ścieżki
        protected int[] bestRoute; //najlepsza ścieżka(o najmniejszym koszcie
        //-------------------------------------------------------------------------------------
        private int whichCity;//które miasto z kolei odwiedzone, zmienna pomocnicza
        public int iteration;
        private int upperLimit;//górne ograniczenie
        private int lowerLimit;//dolne ograniczenie
        int deep=0;
        //-------------------------------------------------------------------------------------
        protected BNBNode topNode;
        protected List<BNBNode> listOfNodes;//lista wierzchołków ze ścieżkami
        protected List<int> tmpRoute; //droga
        int NG;
        //-------------------------------------------------------------------------------------

        /// <summary>
        /// Konstruktor przygotowywujący do wykonania B&B.
        /// </summary>
        /// <param name="gr"> - graf na którym będzie wykonana operacja B&B.</param>
        public BranchAndBound(Graph gr)
        {
            g = new Graph(gr);
            NG = 0;
            //NG = 5;
            bestRoute = new int[g.size];

            bestCost = int.MaxValue;
            upperLimit = int.MaxValue;
            lowerLimit = 0;
            startTop = 0;
            tmpCost = 0;
            whichCity = 0;//początkowe miasto
            iteration = 0;

            listOfNodes = new List<BNBNode>();
            tmpRoute = new List<int>();

            //pirwsza redukcja i ustalenie kosztu w wierzchołku początkowym
            int[,] tmpMatrix = gr.cityMatrix;
            int r = ReduceMatrix(tmpMatrix);
            List<int> t = new List<int>();
            t.Add(0);

            topNode = new BNBNode(r, t, tmpMatrix, g.size, 0);
            listOfNodes.Add(topNode);
            //Console.Write("Początkowy wierzchołek: ");
            //topNode.ShowNode();//ok
            BranchAndBoundFunction(topNode);
            MainLoop();
        }

        /// <summary>
        /// Funkcja, w której jest wykonywany pojedynczy krok algorytmu B&B.
        /// </summary>
        /// <param name="mNode"> - wierzchołek, na którym są przeprowadzany algorytm.</param>
        public void BranchAndBoundFunction(BNBNode mNode)
        {
            int[,] tmpMatrix = new int[mNode.N, mNode.N];//macierz na której będą dokonywane operacje, ciągle przywracana
            int[] costToV = new int[mNode.N];//koszty dojścia do nieodwiedzonych wierzchołków
            int r = 0; //koszt redukcji
            int cost = 0;//koszt drogi

            List<int> route = new List<int>();

            for(int i=0; i< mNode.N; i++)
                for(int j=0; j< mNode.N; j++)
                    tmpMatrix[i, j] = 0;

            for (int i = 0; i < mNode.N; i++)
                costToV[i] = -1;

            mNode.hasChildren = true;
            for (int i = 0; i < mNode.N; i++)
            {
                route.Clear();
                foreach (var el in mNode.tmpRoute)
                    route.Add(el);

                r = 0;
                cost = 0;

                RestoreMatrix(tmpMatrix, mNode.nodeMatrix);

                if (mNode.isVisited[i] == false)//drogi do nieodwiedzonych wierzchołków
                {
                    //dodaje do listy wierzchołek nowy z kosztem, macierzą po redukcji i trasą
                    route.Add(i);
                    InfOnRoute(mNode.V, i, tmpMatrix);
                    r = ReduceMatrix(tmpMatrix);

                    if (mNode.nodeMatrix[mNode.V, i] >= 0)
                        cost = mNode.nodeMatrix[mNode.V, i] + r + mNode.tmpCost;
                    else
                        cost = 0 + r + mNode.tmpCost;

                    listOfNodes.Add(new BNBNode(cost, route, tmpMatrix, mNode.N, i));

                    costToV[i] = cost;
                }
            }

            int min = 0;
            for (int i = 0; i < mNode.N; i++)
                if (costToV[i] >= 0)
                {
                    min = costToV[i];
                    break;
                }

            for (int i = 0; i < mNode.N; i++)
                if (costToV[i] < min && costToV[i] >= 0)
                    min = costToV[i];

            lowerLimit = min;

            //Console.WriteLine("LoweBand: " + lowerLimit);
            //ShowList();
            //potem wyszukać używająć upper i lower banda
            //MainLoop();
        }

        /// <summary>
        /// Funkcja, która zawiera główną pętlę wykonania algorytmu B&B.
        /// </summary>
        public void MainLoop()
        {
            do
            {
                //Console.WriteLine("=================================================="); //odk
                //Console.WriteLine("Main loop");
                //Console.WriteLine("Sort po tmpCost");
                //listOfNodes.Sort((p1, p2) => p1.tmpCost.CompareTo(p2.tmpCost));//układam wierzchołki tak aby ten z najmniejszym kosztem był na górze
                //ShowList();

                //Console.WriteLine("Sort po hasChildren--------------------------------------------");
                //listOfNodes.Sort((p1, p2) => p1.hasChildren.CompareTo(p2.hasChildren));//układam wierzchołki tak aby te bez dzieci były na górze

                //ShowList(); //odk
                
                //Console.WriteLine("warunek: " + (listOfNodes[0].howMuch <= g.size - 2));
                listOfNodes = listOfNodes.OrderBy(o => o.hasChildren).ThenBy(o => o.tmpCost).ToList();
                //inny sposób, przeszukuję listę już posortowaną po kosztach, zliczam ile na początku jest elementów mające dzieci, dzięki temu mam pierwszy element bez nich
                BranchAndBoundFunction(listOfNodes[0]);//debug

                //for(int e=0;e<listOfNodes.Count; e++)
                //{
                //    if (listOfNodes[e].hasChildren)
                //        listOfNodes.Remove(listOfNodes[e]);
                //}
            } while (listOfNodes[0].howMuch < g.size);

            //Console.WriteLine("Koniec: ");
            //ShowList();
            bestCost = listOfNodes[0].tmpCost;
            int i = 0;
            foreach(var el in listOfNodes[0].tmpRoute)
            {
                bestRoute[i] = el;
                i++;
            }

        }

        /// <summary>
        /// Funkcja wyświetlająca listę wszystkich wierzchołków wraz z ich informacjami.
        /// </summary>
        public void ShowList()
        {
            Console.WriteLine("Lista nodów: ");
            foreach (var e in listOfNodes)
                e.ShowNode();
        }
        
        /// <summary>
        /// Funkcja wyświetlająca macierz.
        /// </summary>
        /// <param name="matrix"> - macierz, która zostanie wyświetlona.</param>
        /// <param name="napis"> - napis, który zostanie wyświetlony przed macierzą.</param>
        public void ShowMatrix(int[,] matrix, string napis)
        {
            Console.WriteLine(napis);
            for (int i = 0; i < g.size; i++) 
            {
                for (int j = 0; j < g.size; j++)
                {
                    Console.Write("{0,2} ", matrix[i, j]);
                    //Console.Write(matrix[i, j] + " ");
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }
        
        /// <summary>
        /// Funkcja wpisująca wartości -1(inf) na drogach w macierzy.
        /// </summary>
        /// <param name="from"> - początek drogi.</param>
        /// <param name="to"> - koniec drogi.</param>
        /// <param name="tmpMatrix"> - tablica, która zostanie poddana modyfikacji.</param>
        public void InfOnRoute(int from, int to, int[,] tmpMatrix)
        {
            for (int i = 0; i < g.size; i++)
                tmpMatrix[from, i] = -1;
            for (int i = 0; i < g.size; i++)
                tmpMatrix[i, to] = -1;
            tmpMatrix[to, 0] = -1; //cykl sam na siebie
            tmpMatrix[to, from] = -1;
            //tmpMatrix[from, to] = -1;
        }

        /// <summary>
        /// Funkcja kopiująca wartość macierzy rMatrix do tmpMatrix.
        /// </summary>
        /// <param name="tmpMatrix"> - tablica do której kopiujemy dane.</param>
        /// <param name="rMatrix"> - tablica z której kopiujemy dane.</param>
        public void RestoreMatrix(int[,] tmpMatrix, int[,] rMatrix)
        {
            for (int i = 0; i < g.size; i++)
                for (int j = 0; j < g.size; j++)
                {
                    tmpMatrix[i, j] = 0;
                    tmpMatrix[i, j] = 0 + rMatrix[i, j];
                }
        }

        /// <summary>
        /// Funkcja przeprowadzająca redukcję podanej macierzy po wierszach i kolumnach.
        /// </summary>
        /// <param name="tmpMatrix"> - tablica na której będzie dokonywana redukcja, zostaje ona zmodyfikowana podczas obliczeń.</param>
        /// <returns>Wartość kosztu redukcji.</returns>
        public int ReduceMatrix(int[,] tmpMatrix)
        {
            int min = 0;
            int r = 0;

            //przeszukuje po kolumnach
            //szukam w każdym wierszu wartości minimalnej(jak będzie 0 to przestaję)
            for (int i = 0; i < g.size; i++)
            {
                min = 0;
                for (int j = 0; j < g.size; j++)//pierwsza wartość w tabeli
                    if (tmpMatrix[i, j] >= 0)
                    {
                        min = tmpMatrix[i, j];
                        //Console.WriteLine("Początkowe min: " + min);
                        break;
                    }

                for (int j = 0; j < g.size; j++)
                {
                    if (tmpMatrix[i, j] < min && tmpMatrix[i, j] >= 0)
                    {
                        min = tmpMatrix[i, j];
                    }
                }
                //Console.WriteLine("Znalezione min: " + min + "\n\n\n\n");

                if (min > 0)
                {
                    for (int j = 0; j < g.size; j++)//redukcja
                    {
                        if (tmpMatrix[i, j] > 0)
                        {
                            //Console.WriteLine(tmpMatrix[i, j] + " - " + min + " = " + (tmpMatrix[i, j] - min));
                            tmpMatrix[i, j] -= min;
                        }
                    }
                    r += min;
                }
                //Console.WriteLine();
            }

            //szukam w każdej kolumnie wartości minimalnej 
            for (int j = 0; j < g.size; j++)
            {
                min = 0;
                for (int i = 0; i < g.size; i++)
                    if (tmpMatrix[i, j] >= 0)
                    {
                        min = tmpMatrix[i, j];
                        //Console.WriteLine("Początkowe min: " + min);
                        break;
                    }

                for (int i = 0; i < g.size; i++)
                {
                    if (tmpMatrix[i, j] < min && tmpMatrix[i, j] >= 0)
                    {
                        min = tmpMatrix[i, j];
                    }
                }

                if (min > 0)
                {
                    for (int i = 0; i < g.size; i++)//redukcja
                    {
                        if (tmpMatrix[i, j] > 0)
                        {
                            tmpMatrix[i, j] -= min;
                        }
                    }
                    r += min;
                }
            }
            return r;
        }
       
        /// <summary>
        /// Funkcja sprawdzająca czy wszystkie miasta zostały odwiedzone.
        /// </summary>
        /// <returns>True - gdy wszystko zostało odwiedzone, False - gdy jakieś miasto zostało nieodwiedzone.</returns>
        protected bool isAllCitiesVisited()
        {
            for (int i = 0; i < g.size; i++)
            {
                if (!visitedCities[i])
                    return false;
            }
            return true;
        }

        /// <summary>
        /// Funkcja wyświetlająca całą drogę najkrótszego cyklu Hamiltonowskiego, kończy się na punkcie początkowym. 
        /// </summary>
        public void showBestWay(int[,] matrix)
        {
            NG = 0;
            for (int i = 0; i < g.size; i++)
            {
                if (i < g.size - 1)
                    Console.Write(bestRoute[i] + " - ");
                else
                    Console.Write(bestRoute[i] + " - " + bestRoute[0] + "\n");
                NG++;
            }

            int suma = 0;
            int v = 0;
            for (int i = 1; i < NG; i++)
            {
                //Console.Write("v: " + v + ", i: " + i + ", ");
                //Console.WriteLine(bestRoute[v] + " + " + bestRoute[i] + " = " + matrix[bestRoute[v], bestRoute[i]]);
                suma += matrix[bestRoute[v], bestRoute[i]];
                v = i;
            }
            //Console.Write("v: " + v + ", i: " + 0 + ", ");
            //Console.WriteLine(bestRoute[v] + " + " + bestRoute[0] + " = " + matrix[bestRoute[v], bestRoute[0]]);
            suma += matrix[bestRoute[v], bestRoute[0]];

            Console.WriteLine("Najkrótszy cykl Hamiltonowski o koszcie: " + suma);
        }
    }
}
