﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Diagnostics;

/// <summary>
/// Projektowanie efektywnych algorytmów : projekt 1
/// Autor: Przemysław Wujek, 234983
/// Prowadzący: Dr inż. Jarosław Mierzwa
/// Termin: Czwartek 7:30
/// </summary>

namespace Projekt_1_PEA_Jaroslaw_Mierzwa
{
    class Program
    {
        static void Main(string[] args)
        {
            string nazwa = "text.txt";

            Graph g = null;

            int odp = 0;
            while (true)
            {
                Console.Clear();
                Console.WriteLine("f zegara: " + Stopwatch.Frequency);
                Console.WriteLine("Menu główne");
                Console.WriteLine("0.Wyjście.\n1.Wyświetl wczytaną macierz.\n2.Wprowadź macierz z pliku .txt.\n3.Wyświetl macierz sąsiedztwa.");
                Console.WriteLine("4.Generuj losowy graf.");
                Console.WriteLine("5.Brute Force dla problemu komiwojażera.\n6.Branch and Bound dla problemu komiwojażera.\n7.Programowanie dynamiczne.");
                Console.WriteLine("8.Testy.");

                if (!Int32.TryParse(Console.ReadLine(), out odp))
                {
                    Console.WriteLine("Wprowadziłeś złą opcje, spróbuj ponownie.");
                    Console.ReadLine();
                }
                else
                {
                    switch (odp)
                    {
                        case 0:
                            System.Diagnostics.Process.GetCurrentProcess().Kill();
                            break;
                        case 1:
                            if (g != null) 
                                g.drawMatrix();
                            Console.ReadLine();
                            break;
                        case 2:
                            g = null;//usunięcie starego grafu
                            Console.WriteLine("Podaj nazwę pliku z macierzą: ");
                            nazwa = Console.ReadLine();

                            g = new Graph(nazwa, g);
                            g.drawMatrix();
                            Console.ReadLine();
                            break;
                        case 3:
                            if (g != null)
                                g.drawNeighborhoodMatrix();
                            break;
                        case 4:
                            int size = 7, maxW = 1, minW = 6;
                            bool symetric = false;

                            Console.WriteLine("Podaj rozmiar grafu:");
                            if(!int.TryParse(Console.ReadLine(), out size))
                            {
                                Console.WriteLine("Wprowadziłeś złe dane, spróbuj ponownie.");
                                break;
                            }
                            Console.WriteLine("Podaj minimalną wagę grafu:");
                            if (!int.TryParse(Console.ReadLine(), out minW))
                            {
                                Console.WriteLine("Wprowadziłeś złe dane, spróbuj ponownie.");
                                break;
                            }
                            Console.WriteLine("Podaj maksymalną wagę grafu:");
                            if (!int.TryParse(Console.ReadLine(), out maxW))
                            {
                                Console.WriteLine("Wprowadziłeś złe dane, spróbuj ponownie.");
                                break;
                            }

                            if (g == null)
                                g = new Graph(size, maxW, minW, symetric);
                            else
                            {
                                g = null;
                                g = new Graph(size, maxW, minW, symetric);
                            }

                            g.drawMatrix();
                            Console.ReadKey();
                            break;
                        case 5:
                            //nazwa = "kartka.txt";//do szybkiego testowania
                            if (g == null)
                            {
                                Console.WriteLine("Wczytaj, bądź wygeneruj graf");
                                Console.ReadKey();
                                break;
                            }
                            //g = new Graph(nazwa, g);
                            int vp = 0;//wierzchołek początkowy
                            BruteForce b = new BruteForce(g);
                            Console.WriteLine("Uruchomienie znalezienia cyklu Hamiltona.");

                            Stopwatch sw = new Stopwatch();
                            sw.Start();
                            b.FindRoute(vp);
                            sw.Stop();

                            //g.drawMatrix();
                            //Console.WriteLine("Wyświetlenie drogi.");
                            b.showBestWay();
                            Console.WriteLine("Czas wykonania: " + sw.Elapsed);
                            //Console.WriteLine("Ilość iteracji: " + b.iteration);

                            Console.ReadKey();
                            break;
                        case 6:
                            if (g == null)
                            {
                                Console.WriteLine("Wczytaj, bądź wygeneruj graf");
                                Console.ReadKey();
                                break;
                            }
                            //Graph gz = new Graph(g);

                            int[,] matrixToWrite = new int[g.size, g.size];
                            for (int i = 0; i < g.size; i++)
                            {
                                for (int j = 0; j < g.size; j++)
                                {
                                    matrixToWrite[i, j] = 0;
                                    matrixToWrite[i, j] += g.cityMatrix[i, j];
                                }
                            }

                            Stopwatch swbab = new Stopwatch();
                            swbab.Start();
                            BranchAndBound bab = new BranchAndBound(g);
                            //bab.FindRoute(0);
                            //bab.FindWay(0, g.cityMatrix);
                            swbab.Stop();
                            bab.showBestWay(matrixToWrite);
                            Console.WriteLine("Czas wykonania: " + swbab.Elapsed);

                            for (int i = 0; i < g.size; i++)
                                for (int j = 0; j < g.size; j++)
                                {
                                    g.cityMatrix[i, j] = 0;
                                    g.cityMatrix[i, j] += matrixToWrite[i, j];
                                }
                                
                            //Console.WriteLine("Ilość iteracji: " + bab.iteration);
                            Console.ReadKey();
                            break;
                        case 7:
                            //programowanie dynamiczne, https://en.wikipedia.org/wiki/Held%E2%80%93Karp_algorithm?fbclid=IwAR2OuWktWVn3bIInAJHlDrj3AzJAWcgKNLtunfm-bmrU93PMINxSXDlDNbc
                            break;
                        case 8:
                            Testy();

                            break;
                        default:
                            Console.WriteLine("Wybrałeś złą opcję, spróbuj jeszcze raz.");
                            break;
                    }
                }
            }

            void Testy()
            {
                string fileToSave = "testy.txt";
                int option = 0, howMany = 0, howBig = 0, maxW = 0, minW = 0;
                //TimeSpan time = new TimeSpan(0, 0, 0, 0, 0);
                long time = 0;

                Console.WriteLine("Podaj nazwę pliku do zapisu:");
                fileToSave = Console.ReadLine();

                if(!fileToSave.Contains(".txt"))
                {
                    int s = fileToSave.Length;
                    fileToSave = fileToSave.Insert(s, ".txt");
                }

                Console.WriteLine("Podaj, który algorytm chcesz przetestować(0-BF, 1-B&B, 2-DP):");
                if (!int.TryParse(Console.ReadLine(), out option))
                {
                    Console.WriteLine("Wprowadziłeś złe dane, spróbuj ponownie.");
                    goto ExitFunction;
                }
                Console.WriteLine("Podaj jak wiele razy chcesz powtórzyć:");
                if (!int.TryParse(Console.ReadLine(), out howMany))
                {
                    Console.WriteLine("Wprowadziłeś złe dane, spróbuj ponownie.");
                    goto ExitFunction;
                }
                Console.WriteLine("Podaj jaka wielki ma być graf:");
                if (!int.TryParse(Console.ReadLine(), out howBig))
                {
                    Console.WriteLine("Wprowadziłeś złe dane, spróbuj ponownie.");
                    goto ExitFunction;
                }
                Console.WriteLine("Podaj ograniczenie dolne wagi grafu:");
                if (!int.TryParse(Console.ReadLine(), out minW))
                {
                    Console.WriteLine("Wprowadziłeś złe dane, spróbuj ponownie.");
                    goto ExitFunction;
                }
                Console.WriteLine("Podaj ograniczenie górne grafu:");
                if (!int.TryParse(Console.ReadLine(), out maxW))
                {
                    Console.WriteLine("Wprowadziłeś złe dane, spróbuj ponownie.");
                    goto ExitFunction;
                }

                int vp = 0;//wierzchołek początkowy

                for (int i=0; i<howMany; i++)
                {
                    Console.WriteLine("Powtorzenie nr: " + (i + 1));
                    g = new Graph(howBig, maxW, minW, false);

                    switch(option)//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@zmienić sumowanie czasów
                    {
                        case 0:
                            BruteForce b = new BruteForce(g);
                            Stopwatch sw = new Stopwatch();
                            sw.Start();
                            b.FindRoute(vp);
                            sw.Stop();

                            //b.showBestWay();
                            //Console.WriteLine("Czas wykonania: " + sw.ElapsedMilliseconds);
                            //Console.WriteLine("Ilość iteracji: " + b.iteration);
                            time += sw.ElapsedTicks;
                            break;
                        case 1:
                            BranchAndBound bab = new BranchAndBound(g);

                            Stopwatch swbab = new Stopwatch();
                            swbab.Start();
                            //bab.FindRoute(vp);
                            swbab.Stop();
                            //bab.showBestWay();
                            //Console.WriteLine("Czas wykonania: " + swbab.ElapsedMilliseconds);
                            //Console.WriteLine("Ilość iteracji: " + bab.iteration);
                            //Console.ReadKey();
                            time += swbab.ElapsedTicks;
                            break;
                        case 2:
                            break;
                        default:
                            Console.WriteLine("Coś poszło nie tak.");
                            break;
                    }
                }

            ExitFunction:
                time /= howMany;
                System.IO.File.WriteAllText(@"" + fileToSave, "Czas dla " + howMany + " powtórzeń instancji o wielkości " + howBig + " wynosi: " + time + "\n");
                //dorobić zapisywanie wyników, w nazwie dawać NazwaAlgorytmu_rozmiar_powtórzenia.txt
                //do szybkich wyników zrobić testy automatycznie dla N = 6, 7, 9, 10, 12, 13, 14, 15
                Console.WriteLine("Koniec testów");
                Console.ReadLine();
            }
        }
    }
}
