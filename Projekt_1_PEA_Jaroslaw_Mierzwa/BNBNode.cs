﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Projektowanie efektywnych algorytmów : projekt 1
/// Autor: Przemysław Wujek, 234983
/// Prowadzący: Dr inż. Jarosław Mierzwa
/// Termin: Czwartek 7:30
/// Wierzchołeki dla rozpisanych przejść w grafie
/// </summary>

namespace Projekt_1_PEA_Jaroslaw_Mierzwa
{
    class BNBNode
    {
        public int tmpCost;//koszt dla każdego wierzchołka
        public List<int> tmpRoute;//droga do danego wierzchołka o koszcie w zmniennej wyżej     // = new List<int>();
        public int[,] nodeMatrix;//tablica przejść do miast dla każdego wierzchołka już po redukcji
        public int N; //rozmiar grafu
        public int V; //nr wierzchołka
        public int howMuch;//ilość miast w grafie(samo się liczy)
        public bool[] isVisited;//tablica boolowska odwiedzin w miastach, po niech będę iterować i liczyć koszty(samo się wykonuje)
        public bool hasChildren;//zmienna pomocnicza do sprawdzania czy węzeł ma dzieci, aby porównywać moje liście 

        /// <summary>
        /// Konstruktor tworzący interesujący nas wierzchołek.
        /// </summary>
        /// <param name="tCost"> - koszt drogi dla danego wierzchołka.</param>
        /// <param name="tRoute"> - droga od wierzchołka startowego do obecnego noda.</param>
        /// <param name="matrix"> - tablica sąsiedztwa po redukcji dla danego noda.</param>
        /// <param name="n"> - rozmiar grafu.</param>
        /// <param name="n"> - numer wierzchołka.</param>
        ///  
        public BNBNode(int tCost, List<int> tRoute, int[,] matrix, int n, int v)
        {
            hasChildren = false;
            tmpCost = 0+tCost;
            //tmpRoute = tRoute;
            N = n;
            V = v;

            tmpRoute = new List<int>();
            nodeMatrix = new int[N, N];

            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < N; j++)
                {
                    nodeMatrix[i, j] = matrix[i, j];
                }
            }

            foreach(var el in tRoute)
            {
                tmpRoute.Add(el);
            }

            howMuch = tmpRoute.Count;
            isVisited = new bool[N];
            for (int i = 0; i < N; i++)
                isVisited[i] = false;

            foreach (var el in tRoute)
                isVisited[el] = true;
        }

        public void ShowNode()
        {
            Console.Write("Wierzchołek: " + V + ", koszt: " + tmpCost + ", czy ma dzieci: " + hasChildren + ", trasa: ");
            foreach (int el in tmpRoute)
            {
                Console.Write("{0,2} ", el);
            }
            Console.WriteLine();
            /*Console.WriteLine("\nMacierz po redukcji: ");
            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < N; j++)
                {
                    Console.Write("{0,2} ", nodeMatrix[i, j]);
                }
                Console.WriteLine();
            }
            Console.WriteLine();*/
        }
    }
}
